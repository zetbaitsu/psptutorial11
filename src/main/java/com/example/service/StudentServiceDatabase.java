package com.example.service;

import com.example.dao.StudentMapper;
import com.example.model.StudentModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class StudentServiceDatabase implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public StudentModel selectStudent(String npm) {
        log.info("select student with npm {}", npm);
        return studentMapper.selectStudent(npm);
    }

    @Override
    public List<StudentModel> selectAllStudents() {
        log.info("select all students");
        return studentMapper.selectAllStudents();
    }

    @Override
    public void addStudent(StudentModel student) {
        log.info("student {}  added", student.getNpm());
        studentMapper.addStudent(student);
    }

    @Override
    public void deleteStudent(String npm) {
        log.info("student {} deleted", npm);
        studentMapper.deleteStudent(npm);
    }

    @Override
    public void updateStudent(StudentModel student) {
        log.info("student {}  updated", student.getNpm());
        studentMapper.updateStudent(student);
    }
}
