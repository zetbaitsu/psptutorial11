package com.example.service;

import com.example.model.CourseModel;

import java.util.List;

public interface CourseService {
    List<CourseModel> selectAllCourses();

    CourseModel selectCourse(String idCourse);
}
